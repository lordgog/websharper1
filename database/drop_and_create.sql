IF DB_ID('padoabi') IS NOT NULL
	DROP DATABASE padoabi;
GO

CREATE DATABASE padoabi;
GO
USE padoabi;
GO

CREATE TABLE posts(
	[id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[content] [varchar](max) NOT NULL,
	[date] [date] NOT NULL);
GO

CREATE TABLE tags(
	[id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[tag] [varchar](50) NOT NULL,
	CONSTRAINT uniqueTags UNIQUE (tag));
GO

CREATE TABLE posttags(
	[id] [int] PRIMARY KEY IDENTITY(1,1) NOT NULL,
	[postId] [int] NOT NULL,
	[tagId] [int] NOT NULL,
	CONSTRAINT posttagsToPosts
	FOREIGN KEY (postId) REFERENCES posts (id),
	CONSTRAINT posttagsToTags
	FOREIGN KEY (tagId) REFERENCES tags (id));
GO

