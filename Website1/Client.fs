namespace Website1

open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Html
open System
open System.Data
open System.Data.Linq
open Microsoft.FSharp.Data.TypeProviders
open Microsoft.FSharp.Linq
open IntelliFactory.WebSharper.Formlet
open IntelliFactory.WebSharper.Formlet.Controls
open FSharp.Markdown

module Formlet =

    type Post =
            {
                Title:string
                Content:string
                Tags:string
            }
    type dbSchema = SqlDataConnection<ConnectionStringName="ConnectionString">
    let containsTag tag list = List.exists (fun elem -> elem = tag) list

    [<JavaScript>]
    module Client =
        
        let NewPostFormlet : Formlet<Post> =
            let titleF =
                Input ""
                |> Validator.IsNotEmpty ""
                |> Enhance.WithTextLabel "Title"
            let contentF =
                TextArea ""
                |> Validator.IsNotEmpty ""
                |> Enhance.WithTextLabel "Content"
            let tagsF =
                Input ""
                |> Validator.IsNotEmpty ""
                |> Enhance.WithTextLabel "Tags"
            Formlet.Yield (fun title content tags ->
                {Title = title; Content = content; Tags = tags})
            <*> titleF
            <*> contentF
            <*> tagsF


    let getPosts() =
        let db = dbSchema.GetDataContext()
        let query1 = query { for row in db.Posts do select row}
        seq { for i in query1 do yield [i.Id.ToString(); i.Title]}

    let getPost postId =
        let db = dbSchema.GetDataContext()
        let query1 = query { for row in db.Posts do
                             where (row.Id = Int32.Parse(postId))
                             select row
                             exactlyOne}
        [query1.Title; query1.Content]
        
    
    let getTaggedPosts (x:string) =
            let db = dbSchema.GetDataContext()
            let query1 = query {
                for row1 in db.Posts do
                    join row2 in db.Posttags on (row1.Id = row2.PostId)
                    join row3 in db.Tags on (row2.TagId = row3.Id)
                    where (  row3.Tag = x )
                    select row1
            }
            seq { for i in query1 do yield [i.Id.ToString(); i.Title]}

    let getTags() =
        let db = dbSchema.GetDataContext()
        let query1 = query {
            for row in db.Tags do
            select row
        }
        seq { for i in query1 do yield [i.Tag]}

    let getPostTags(x:string) =
        let db = dbSchema.GetDataContext()
        let query1 = query {
            for row1 in db.Tags do
            join row2 in db.Posttags on (row1.Id = row2.TagId)
            where (row2.PostId = Int32.Parse(x))
            select (row1)
        }
        [ for i in query1 do yield i.Tag ]
        

    [<Rpc>]        
    let SubmitNewPost (post : Post) =
            let tags = post.Tags.Split [|' '|]
            for i in tags do
                Console.WriteLine(i)
            let db = dbSchema.GetDataContext()
            db.DataContext.Log <- System.Console.Out
            let newTags = [for i in tags -> new dbSchema.ServiceTypes.Tags(Tag = i)]
            let CurrentTags = query {
                for row in db.Tags do
                select row}
            let x = [for i in CurrentTags do yield i.Tag]
            for i in newTags do
                if not(List.exists(fun elem -> elem = i.Tag) x) then
                    db.Tags.InsertOnSubmit(i)
            try
                db.DataContext.SubmitChanges()
                printfn  "Tags inserted."
            with
                | exn -> printfn "Error while inserting tags \n%s" exn.Message

            let parsed = Markdown.Parse(post.Content)
            let html = Markdown.WriteHtml(parsed)
            let newPost = new dbSchema.ServiceTypes.Posts(
                                                    Title = post.Title,
                                                    Content = Markdown.TransformHtml(post.Content),
                                                    Date = DateTime.UtcNow)
            db.Posts.InsertOnSubmit(newPost)
            try
                db.DataContext.SubmitChanges()
                printfn "Post inserted."
            with
                | exn -> printfn "Error while inserting post: \n%s" exn.Message
        
            let queryPostId = query {
                for row in db.Posts do
                where (row.Title = post.Title)
                select row.Id
                exactlyOne
            }

            let queryTagIds = [for i in tags -> query { for row in db.Tags do
                                                        where (row.Tag = i)
                                                        select row.Id
                                                        exactlyOne}]
            let NewPostTags = [for i in queryTagIds -> dbSchema.ServiceTypes.Posttags(TagId = i, PostId = queryPostId)]
            db.Posttags.InsertAllOnSubmit(NewPostTags)
            try
                db.DataContext.SubmitChanges()
                printfn "New post tags successfully inserted."
            with
                | exn -> printfn "Error while inserting post tags: \n%s" exn.Message
            

    [<JavaScript>]
    let Main () =
        let fc = {
            Enhance.FormContainerConfiguration.Default with
                Header = "New Post" |> Enhance.FormPart.Text |> Some
                Description = "Make a new post"
                |> Enhance.FormPart.Text
                |> Some
            }
        let f =
            Client.NewPostFormlet 
            |> Enhance.WithSubmitButton
            |> Enhance.WithCustomFormContainer fc
        Formlet.Run (fun post -> SubmitNewPost post) f

type FormletViewer() =
    inherit Web.Control()
    [<JavaScript>]
    override this.Body = Formlet.Main ()
