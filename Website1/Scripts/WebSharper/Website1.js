(function()
{
 var Global=this,Runtime=this.IntelliFactory.Runtime,WebSharper,Formlet,Controls,Data,Enhance,Formlet1,FormContainerConfiguration,Website1,Formlet2,Client,Remoting;
 Runtime.Define(Global,{
  Website1:{
   Formlet:{
    Client:{
     NewPostFormlet:Runtime.Field(function()
     {
      var titleF,x,x1,f,f1,contentF,x2,x3,f2,f3,tagsF,x4,x5,f4,f5;
      titleF=(x=(x1=Controls.Input(""),(f=function(arg10)
      {
       return Data.Validator().IsNotEmpty("",arg10);
      },f(x1))),(f1=function(formlet)
      {
       return Enhance.WithTextLabel("Title",formlet);
      },f1(x)));
      contentF=(x2=(x3=Controls.TextArea(""),(f2=function(arg10)
      {
       return Data.Validator().IsNotEmpty("",arg10);
      },f2(x3))),(f3=function(formlet)
      {
       return Enhance.WithTextLabel("Content",formlet);
      },f3(x2)));
      tagsF=(x4=(x5=Controls.Input(""),(f4=function(arg10)
      {
       return Data.Validator().IsNotEmpty("",arg10);
      },f4(x5))),(f5=function(formlet)
      {
       return Enhance.WithTextLabel("Tags",formlet);
      },f5(x4)));
      return Data.$(Data.$(Data.$(Formlet1.Return(function(title)
      {
       return function(content)
       {
        return function(tags)
        {
         return{
          Title:title,
          Content:content,
          Tags:tags
         };
        };
       };
      }),titleF),contentF),tagsF);
     })
    },
    Main:function()
    {
     var fc,inputRecord,Description,arg0,f,arg02,f1,f2,formlet,formlet1;
     fc=(inputRecord=FormContainerConfiguration.get_Default(),(Description=(arg0=(f=function(arg01)
     {
      return{
       $:0,
       $0:arg01
      };
     },f("Make a new post")),{
      $:1,
      $0:arg0
     }),Runtime.New(FormContainerConfiguration,{
      Header:(arg02=(f1=function(arg01)
      {
       return{
        $:0,
        $0:arg01
       };
      },f1("New Post")),{
       $:1,
       $0:arg02
      }),
      Padding:inputRecord.Padding,
      Description:Description,
      BackgroundColor:inputRecord.BackgroundColor,
      BorderColor:inputRecord.BorderColor,
      CssClass:inputRecord.CssClass,
      Style:inputRecord.Style
     })));
     f2=(formlet=(formlet1=Client.NewPostFormlet(),Enhance.WithSubmitButton(formlet1)),Enhance.WithCustomFormContainer(fc,formlet));
     return Formlet1.Run(function(post)
     {
      return Remoting.Send("Website1:0",[post]);
     },f2);
    }
   },
   FormletViewer:Runtime.Class({
    get_Body:function()
    {
     return Formlet2.Main();
    }
   })
  }
 });
 Runtime.OnInit(function()
 {
  WebSharper=Runtime.Safe(Global.IntelliFactory.WebSharper);
  Formlet=Runtime.Safe(WebSharper.Formlet);
  Controls=Runtime.Safe(Formlet.Controls);
  Data=Runtime.Safe(Formlet.Data);
  Enhance=Runtime.Safe(Formlet.Enhance);
  Formlet1=Runtime.Safe(Formlet.Formlet);
  FormContainerConfiguration=Runtime.Safe(Enhance.FormContainerConfiguration);
  Website1=Runtime.Safe(Global.Website1);
  Formlet2=Runtime.Safe(Website1.Formlet);
  Client=Runtime.Safe(Formlet2.Client);
  return Remoting=Runtime.Safe(WebSharper.Remoting);
 });
 Runtime.OnLoad(function()
 {
  Client.NewPostFormlet();
 });
}());
