declare module Website1 {
    module Skin {
        interface Page {
            Title: string;
            Body: _List.T<_Html.Element<_Web.Control>>;
        }
    }
    module Formlet {
        module dbSchema {
            module ServiceTypes {
                module SimpleDataContextTypes {
                    interface Blog {
                    }
                }
                interface Posts {
                }
                interface Posttags {
                }
                interface Tags {
                }
                interface SimpleDataContextTypes {
                }
            }
            interface ServiceTypes {
            }
        }
        module Client {
            var NewPostFormlet : {
                (): _Data.Formlet<any>;
            };
        }
        interface dbSchema {
        }
        interface Post {
            Title: string;
            Content: string;
            Tags: string;
        }
        var Main : {
            (): _Html1.IPagelet;
        };
    }
    interface FormletViewer {
        get_Body(): _Html1.IPagelet;
    }
    interface Action {
    }
    interface Website {
    }
    
    import _List = IntelliFactory.WebSharper.List;
    import _Html = IntelliFactory.Html.Html;
    import _Web = IntelliFactory.WebSharper.Web;
    import _Data = IntelliFactory.WebSharper.Formlet.Data;
    import _Html1 = IntelliFactory.WebSharper.Html;
}
