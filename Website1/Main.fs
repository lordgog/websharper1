namespace Website1

open IntelliFactory.Html
open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Sitelets

type Action =
    | NewPost
    | Tagek of string
    | Page of string


    (*
module Controls =

    [<Sealed>]
    type MyControl(s:string) =
        inherit Web.Control()

        [<JavaScript>]
        override this.Body = Formlet.Main()
        *)

module Skin =
    open System.Web

    type Page =
        {
            Title : string
            Body : list<Content.HtmlElement>
        }

    let MainTemplate =
        Content.Template<Page>("~/Main.html")
            .With("title", fun x -> x.Title)
            .With("body", fun x -> x.Body)

    let WithTemplate title body : Content<Action> =
        Content.WithTemplate MainTemplate <| fun context ->
            {
                Title = title
                Body = body context
            }

module Pages =
    let ( => ) text url =
        A [HRef url] -< [Text text]
    let Links (ctx: Context<Action>) =
        Div [
            H4 [Class "menu"] -< [MarginWidth "10"] -< ["New Post" => ctx.Link NewPost]
            H4 [Class "menu"] -< ["Tags" => ctx.Link (Tagek "")]
            H4 [Class "menu"] -< ["Browse" => ctx.Link (Page "")]
            HR []
        ]
    
    let Page (s:string) =
        if s.Length = 0 then
            let posts = Website1.Formlet.getPosts()
            Skin.WithTemplate "Page browse" <| fun ctx ->
            [
                Links ctx
                Div [Text "Postok:" ] -<
                [
                    for i in posts ->
                        Div [
                            P [A [HRef ("/Page/" + i.[0])] -< [Text i.[1]]]
                        ]
                ]
            ]
        else
            let post = Website1.Formlet.getPost(s)
            let tags = Website1.Formlet.getPostTags(s)
            Skin.WithTemplate "Read" <| fun ctx ->
            [
                Links ctx
                Div [] -<
                [
                    P [H1 [Text ("Title: " + post.[0])]]
                    P [VerbatimContent post.[1]]
                ]
                Div [Text "Tagek:"] -<
                [
                    for i in tags ->
                        H6 [Class "tagek"] -< [A [HRef ("/Tagek/" + i)] -< [Text i]]
                ]
            ]

    let NewPostPage =
        Skin.WithTemplate "NewPost" <| fun ctx ->
        [
            Links ctx
            Div [new Website1.FormletViewer()]
        ]
    let TagPage (s:string) =
        if s.Length = 0 then
            let tags = Website1.Formlet.getTags()
            Skin.WithTemplate "Tag" <| fun ctx ->
            [   
                Links ctx
                P [Text "Tagek:"]
                Div [] -<
                [
                    for i in tags ->
                        P [Class "tagek"] -< [A [HRef ("/Tagek/" + i.[0])] -< [Text i.[0]]]
                ]
            ]
        else
            let posts = Website1.Formlet.getTaggedPosts s
            Skin.WithTemplate "Tag" <| fun ctx ->
            [
                Links ctx
                Div [] -<
                   [
                        Div [Text ("Tag: " + s)]
                        Div [] -<
                        [
                            for i in posts ->
                                Div [] -<
                                [
                                    P [A [HRef ("/Page/" + i.[0])] -< [Text i.[1]]]
                                ]
                        ]
                    ]
            ]

module Site =
    let Main =
        let sub =
            Sitelet.Infer (fun action ->
                match action with
                | Action.NewPost -> Pages.NewPostPage
                | Action.Tagek s -> Pages.TagPage s
                | Action.Page s -> Pages.Page s
            )
        Sitelet.Sum [
            sub
            Sitelet.Content "/" (Action.Page "") (Pages.Page "")
        ]


[<Sealed>]
type Website() =
    interface IWebsite<Action> with
        member this.Sitelet = Site.Main
        member this.Actions = [(Page ""); NewPost; (Tagek "")]

type Global() =
    inherit System.Web.HttpApplication()

    member g.Application_Start(sender: obj, args: System.EventArgs) =
        ()

[<assembly: Website(typeof<Website>)>]
do ()
