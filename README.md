# README #

F# / Websharper class project: implementing basic blog functions.
Created by: Dániel Papp (gogworld@yahoo.com, pdg@inf.elte.hu)
ELTE, Budapest 2014

### Required NuGet packages ###

* WebSharper
* FSharp.Formatting

### Setup ###

* Set up a database using the provided script, located at *~/database/drop_and_create.sql*. Edit the database name in the script if necessary.
* Alternatively, you can use the provided test database backup file, located at *~/database/blog-backup.bak*.
* Edit the database connection string in *~/Website1/Web.config* to match your database. You can read more information about how to set up your connection string [here](http://msdn.microsoft.com/en-us/library/hh361033.aspx#BKMK_SetUpTypeProv) and [here](http://msdn.microsoft.com/en-us/library/hh362320.aspx).
* Build and run.

### How to use, what does it do? ###

The website implements basic blog functions, such as submitting, browsing and reading posts.

On the New Post page you'll be able to provide a title to your post, some content in **[Markdown](http://daringfireball.net/projects/markdown/)** format, and different tags **separated by a single space character**. Each word will count as a different tag to your post. Each field must be filled out before submitting. Submitting a new post will redirect you to the homepage.

On the Tags page, you can browse the various tags of posts. Clicking on one will show you all the posts that have been tagged with the selected item. Clicking on a post will show you it's whole content.

On the Browse page, you can browse through every submitted post's title. Upon clicking one, you can view the whole content.